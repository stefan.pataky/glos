#!/usr/bin/env node
import { MainMenuState } from './states/MainMenuState';
import { State } from './states/State';

export const DATA_PATH = './data/';

export class Program {
    currentState: State

    constructor() {
        // Initial state
        this.currentState = new MainMenuState();
    }
    
    run(): void {
       const next = this.currentState.run()

       if(next) {
        this.currentState = next;
       }

       this.run();
    }
}


const p = new Program();
p.run();
