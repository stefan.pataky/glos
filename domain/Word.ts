
export interface Word {
    left: string;
    right: string;
    answer?: string;
    correct?: boolean;
}
