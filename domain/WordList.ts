import { Word } from "./Word";

export interface WordList {
    name: string;
    words: Word[];
    stats?: Stats[];
}

export interface Stats {
    time: number,
    result: number
}
