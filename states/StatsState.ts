import { State } from './State';
import * as chalk from 'chalk';
import * as readline from 'readline-sync';
import { DATA_PATH } from '../main';
import * as Table from 'cli-table';

import * as fs from 'fs';
import { MainMenuState } from './MainMenuState';
import { WordList } from '../domain/WordList';

export class StatsState extends State {
    run(): State {
        console.clear();
        this.print(chalk.bold('Statistik'))
        
        const files = fs.readdirSync(DATA_PATH);
        const index = readline.keyInSelect(files.map(f => f.replace('.json', '')), 'Välj fil', { cancel: 'tillbaka' });


        if(index === -1) {
            return new MainMenuState();
        }

        const wordlist = JSON.parse(fs.readFileSync(`${DATA_PATH}${files[index]}`, 'utf8')) as WordList;


        const table = new Table({
            head: [chalk.green('datum'), chalk.green('resultat')]
          , colWidths: [30, 20]
        });

        wordlist.stats?.forEach(s => {
            table.push([new Date(s.time).toISOString(), `${Number(s.result * 100).toFixed(1)}%`])
        })


        this.print(table.toString());

        this.question('Fortsätt');
        
        return new StatsState();
    } 

}