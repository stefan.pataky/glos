import * as fs from 'fs';
import { State } from './State';
import { DATA_PATH } from '../main';
import { MainMenuState } from "./MainMenuState";
import * as chalk from 'chalk';
import { WordList } from '../domain/WordList';

export class CreateState extends State {
    constructor() {
        super();
    }

    menu = ['Spara'];

    run(): State {
        this.print(chalk.bold('Skapa ny glos-fil'))

        const name = this.question('Namn');
        const num = this.questionInt('Antal glosor');

        const wordlist: WordList = {
            name,
            words :[]
        }

        let n = 0;
        while(n < num) {
            const left = this.question(`Ord ${n+1}`);
            const right = this.question(`Svar ${n+1}`);
            wordlist.words.push({ left, right});
            n++;
        }

        const index = this.showMenu();

        switch (index) {
            case -1:
                return new MainMenuState();
            case 0:
                // Save file
                if(!fs.existsSync(DATA_PATH)) {
                    fs.mkdirSync(DATA_PATH);
                }
                
                fs.writeFileSync(`${DATA_PATH}${wordlist.name}.json`, JSON.stringify(wordlist));
                return new MainMenuState();
        }
    }
}
