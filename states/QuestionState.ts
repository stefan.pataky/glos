import * as chalk from 'chalk';
import * as readline from 'readline-sync';
import * as fs from 'fs';
import { State } from './State';
import { DATA_PATH } from '../main';
import { ResultState } from './ResultState';
import { WordList } from '../domain/WordList';

export class QuestionState extends State {
    constructor(private file: string) {
        super();
    }

    run(): State {
        console.clear();
        const wordlist = JSON.parse(fs.readFileSync(`${DATA_PATH}${this.file}`, 'utf8')) as WordList;

        this.print(chalk.bold(wordlist.name));

        const idxs = this.shuffle(wordlist.words.map((_, i) => i));

        for(const idx of idxs) {
            const word = wordlist.words[idx];
            const answer = readline.question(`${word.left}? `);

            word.answer = answer;
            word.correct = answer === word.right;

            const feedback = word.correct ? chalk.green('✅') : chalk.red(`❌ Rätt svar: ${chalk.bold(word.right)}`);

            this.print(feedback);
        }

        return new ResultState(this.file, wordlist);
    }

    private shuffle<T>(array: T[]): T[] {
        let currentIndex = array.length;
        let temporaryValue;
        let randomIndex;
      
        while (0 !== currentIndex) {
      
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex -= 1;
      
          temporaryValue = array[currentIndex];
          array[currentIndex] = array[randomIndex];
          array[randomIndex] = temporaryValue;
        }
      
        return array;
      }
}
