import * as readline from 'readline-sync';

export abstract class State {
    menu: string[];

    abstract run(): State;

    print(s: string): void {
       console.log(s);
    }

    showMenu(cancel='Avbryt'): number {
        return readline.keyInSelect(this.menu, 'Meny', { cancel });
    }

    question(q: string): string {
        return readline.question(`${q}? `);
    }
    
    questionInt(q: string): number {
        return readline.questionInt(`${q}? `);
    }
}
