import * as chalk from 'chalk';
import * as figlet from 'figlet';

import { State } from './State';
import { ListState } from './ListState';
import { CreateState } from './CreateState';
import { StatsState } from './StatsState';

export class MainMenuState extends State {
    menu = [
        'Lista',
        'Skapa ny',
        'Statistik'
    ];

    run(): State {
        console.clear();

        this.print(
            chalk.green(
                figlet.textSync('-=Glosförhör 2000=-', { horizontalLayout: 'full' })
            ));

        const index = this.showMenu();

        switch (index) {
            case -1:
                process.exit(0);
                break;
            case 0:
                return new ListState();
            case 1:
                return new CreateState();
            case 2:
                return new StatsState();
        }
    }
}
