import * as chalk from 'chalk';
import { State } from './State';
import { WordList } from '../domain/WordList';
import { ListState } from './ListState';
import { QuestionState } from './QuestionState';
import * as Table from 'cli-table';
import * as fs from 'fs';
import { DATA_PATH } from '../main';

export class ResultState extends State {
    constructor(private file: string, private result: WordList) {
        super();
    }

    menu = ['Försök igen']
    
    run(): State {
        console.clear();

        this.print(chalk.bold('Resultat'))

        const table = new Table({
            head: [chalk.green('ord'), chalk.green('svar'), chalk.green('ditt svar'), chalk.green('rätt?')]
          , colWidths: [20, 20, 20, 20]
        });

        const n = this.result.words.length;
        let correct = 0;

        this.result.words.forEach(word => {
            table.push([word.left,word.right,word.answer, word.correct ? chalk.green('ja') : chalk.red('nej')]);
            if(word.correct) {
                correct++;
            }
        });
        
        this.print(table.toString());
        this.print(chalk.cyan(`${((correct/n)*100).toFixed(1)}% rätt!`))

        if(!this.result.stats) {
            this.result.stats = [];
        }

        this.result.stats.push({ time: new Date().getTime(), result: correct/n})

        //Save
        const json = JSON.stringify(this.result);
        fs.writeFileSync(`${DATA_PATH}${this.result.name}.json`, json);  
        
        const index = this.showMenu();
        switch(index) {
            case -1:
                return new ListState();
            case 0:
                return new QuestionState(this.file);
        }
    }
}
