import * as readline from 'readline-sync';
import * as fs from 'fs';
import { State } from './State';
import { QuestionState } from './QuestionState';
import { DATA_PATH } from '../main';
import { MainMenuState } from "./MainMenuState";
import * as chalk from 'chalk';

export class ListState extends State {
    constructor() {
        super();
    }
    
    run(): State {
        this.print(chalk.bold('Lista'))

        const files = fs.readdirSync(DATA_PATH);
        const index = readline.keyInSelect(files.map(f => f.replace('.json', '')), 'Välj fil', { cancel: 'tillbaka' });

        switch (index) {
            case -1:
                return new MainMenuState();
            default:
                return new QuestionState(files[index]);
        }
    }
}
